# ETF Information Frontend

the front end service for the etf scraper

to run clone the repo, run npm install then run the command

gatsby build; gatsby serve -p 8000

or just run

gatsby develop -p 8000

the website should be fully accessible from your localhost

@ Sam Ingram
