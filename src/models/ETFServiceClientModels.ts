export interface ETFHoldingsResponse {
    data: {
        statusCode: string,
        headers: {
            [key: string]: string
        }
        body: {
            holdings: string[],
            performance: string
        } | string
    }
}
export interface Outperformer {
    ticker: string
    performance: number
    average_volume: number
}
export interface ETFHoldingsInput {
    ticker: string
}

export enum Status {
    "START" = 1,
    "LOADING" = 2,
    "ERROR" = 3,
    'LOADED' = 4
}