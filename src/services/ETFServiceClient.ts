import axios from "axios"
import { ETFHoldingsResponse } from "../models/ETFServiceClientModels"

export default class ETFServiceClient {
    private readonly baseUrl = `https://mx72grwq5j.execute-api.us-east-1.amazonaws.com/dev`
    public async getHoldings (ticker:string, start: string, end: string): Promise<ETFHoldingsResponse> {
        const url = `${this.baseUrl}/getHoldings`
        const body ={
            ticker,
            start,
            end
        }
        return axios.post(url, body, undefined)
    }
    public async getOutperformers (fullHoldings: string[], start: string, end: string, threshold: string): Promise<any> {
        const url = `${this.baseUrl}/getOutperformers`
        const body = {
            holdings: fullHoldings,
            start,
            end,
            threshold
        }
        return axios.post(url,body,undefined).catch(err => {console.log(err)})
    }
}