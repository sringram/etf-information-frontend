import React from "react";
import Header from "../components/Header";
import PerformanceDisplay from "../components/PerformanceDisplay";

export default function HomePage() {
  return (
      <div className="PageLayout">
        <Header />
        <PerformanceDisplay />
      </div>
  );
}
