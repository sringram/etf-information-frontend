export const convertToPercentage = (number: string): string => {
    let tempFloat: number = parseFloat(number)
    return `${((tempFloat*100).toFixed(2))}%`
}