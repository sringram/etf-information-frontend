import React from "react";
import { Outperformer, Status } from "../models/ETFServiceClientModels"
import Ticker from "./Ticker"
import { convertToPercentage } from "../utils/helperFunctions"
import "../css/main.css"

export interface OutperformersProps {
    outperformers: Outperformer[]
    status: Status
    etfPerformance: number
}

export default function Outperformers(props: OutperformersProps) {
    const { etfPerformance } = props
    const sort = (a,b) => {
        if(a.performance > b.performance){
            return -1
        }
        else if(a.performance < b.performance){
            return 1
        }
        return 0
    }
    switch(props.status){
        case Status.START:
            return(
            <div className={'font getStarted'}>
                Click Submit to get started
            </div>
            )
        case Status.LOADING:
            return (
                <div></div>
            )
        case Status.ERROR:
            return (
                <div>
                    Error, try again with different parameters
                </div>
            )
        case Status.LOADED:
            return(
                <>
                    <table className={'Outperformers'}>
                        {
                            props.outperformers.sort(sort).map(op => OutperformerDisplay({...op, etfPerformance}))
                        }
                    </table>
                </>
            )
    }
}

const OutperformerDisplay = ({ticker, performance, average_volume, etfPerformance}) => {
    return (
        <tr key={ticker} className={`Outperformer`}>
            <Ticker ticker={ticker}/>
            <td className={`performanceItem font ${etfPerformance > performance ? 'negative' : `positive`}`}>{`performance: ${convertToPercentage(performance)}`}</td>
            <td className={'volumeItem font'}>{`volume: ${average_volume}`}</td>
        </tr>
    )
}