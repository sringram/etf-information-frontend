import React from "react";

export default function Header() {
    return(
        <div className='Header'>
            <div className="header-text">
                ETF Performance Analyzer
            </div>
        </div>
    )
}