import React from "react"

export interface TickerProps {
    ticker: string
}

const Ticker = (props: TickerProps) => {
    return (
        <td className={"ticker"} onClick={() => window.open(`https://finance.yahoo.com/quote/${props.ticker}/`)}>
            {props.ticker}
        </td>
    )
}
export default Ticker