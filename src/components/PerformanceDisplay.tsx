import React from "react";
import InputForm from "../components/InputForm";
import Outperformers from "../components/Outperformers";
import Performance from "../components/Performance";
import { Status } from "../models/ETFServiceClientModels";

export default function PerformanceDisplay() {
    const [outperformers, setOutperformers] = React.useState([])
    const [etfPerformance, setEtfPerformance] = React.useState(0)
    const [status, setStatus] = React.useState(Status.START)

    return (
        <>
            <InputForm setOutperformers={setOutperformers} setEtfPerformance={setEtfPerformance} setStatus={setStatus}/>
            <div className={"PerformanceHolder"}>
                <Performance etfPerformance={etfPerformance} status={status}/>
            </div>
            <Outperformers outperformers={outperformers} status={status} etfPerformance={etfPerformance}/>
        </>
    );
}
