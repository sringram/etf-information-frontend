import React, { useState } from "react";
import { Outperformer, Status } from "../models/ETFServiceClientModels"
import ETFServiceClient from "../services/ETFServiceClient"

const getDate = () => {
  var currentDate = new Date();
  return `${currentDate.getMonth() + 1}/${currentDate.getDate()}/${currentDate.getFullYear()}`
}

export interface InputFormProps {
  setOutperformers: React.Dispatch<React.SetStateAction<Outperformer[]>>
  setEtfPerformance: React.Dispatch<React.SetStateAction<number>>
  setStatus: React.Dispatch<React.SetStateAction<Status>>
}

export default function InputForm(props: InputFormProps) {
  const { value:ticker, bind:bindTicker, reset:resetTicker } = useInput('DIA')
  const { value:start, bind:bindStart, reset:resetStart } = useInput('1/1/2020')
  const { value:end, bind:bindEnd, reset:resetEnd } = useInput(getDate())
  const [etfServiceClient] = useState(new ETFServiceClient())

  const reset = () => {
    resetTicker()
    resetStart()
    resetEnd()
  }

  const handleSubmit = (event) => {
    props.setStatus(Status.LOADING)
    event.preventDefault()
    if( sanitizeInput(ticker,"Ticker") && 
        sanitizeInput(start,'Date') && 
        sanitizeInput(end,'Date') && 
        (new Date(start) < new Date(end))
      ){
      console.log(`Submitted ${ticker}, ${start} and ${end}, thanks I guess`)
      const outperformers = etfServiceClient.getHoldings(ticker,start,end)
      .then((holdingsResponse) => {
        if(typeof(holdingsResponse.data.body) === "string"){
          props.setStatus(Status.ERROR)
          reset()
          return
        };
        props.setEtfPerformance(parseFloat(holdingsResponse.data.body.performance));
        return etfServiceClient.getOutperformers(holdingsResponse.data.body.holdings, start, end, holdingsResponse.data.body.performance)})
      .then(
        outperformers => {
          if(!outperformers) { 
            throw new Error("Outperformers not found")
          }
          const payload = JSON.parse(outperformers.data.body)
          props.setOutperformers(payload)
          props.setStatus(Status.LOADED)
        })
      .catch(
        ex => {
          props.setStatus(Status.ERROR)
          console.log(ex)
          reset()
        })
    }
    else{
      props.setStatus(Status.ERROR)
      console.log("input format incorrect")
      reset()
    }
  }
  return (
    <div className={'inputInformation'}>
      <form className={'font form'} key={`InputForm`} onSubmit={handleSubmit}>
        <label className={'label'}>
          Ticker
          <input className={'input'} type="text" {...bindTicker} />
        </label>
        <label className={'label'}>
          Start
          <input className={'input'} type="text" {...bindStart} />
        </label>
        <label className={'label'}>
          End
          <input className={'input'} type="text" {...bindEnd} />
        </label>
        <input className={'submit'} type="submit" value="Submit" />
      </form>
    </div>
  )
}
export const useInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(initialValue),
    bind: {
      value,
      onChange: event => {
        setValue(event.target.value);
      }
    }
  }
}
export const sanitizeInput = (input, inputType) => {
  switch(inputType){
    case("Ticker"):
      const TickerRegExp = new RegExp('^[a-zA-Z]{2,4}$')
      return TickerRegExp.test(input)
    case("Date"):
      // this regex should be valid TODO figure out how to regex check the dates https://stackoverflow.com/questions/15491894/regex-to-validate-date-format-dd-mm-yyyy
      // const DateRegExp = new RegExp(`^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$`)
      // return DateRegExp.test(input)
      //for now basic sanitization
      return (new Date(input) <= new Date(Date.now()))
    default:
      return false
  }
}