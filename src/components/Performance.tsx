import React from "react";
import { Status } from "../models/ETFServiceClientModels"
import { convertToPercentage } from "../utils/helperFunctions"
import "../css/main.css"

export interface ETFPerformanceProps {
    etfPerformance: number
    status: Status
}

export default function Performance(props: ETFPerformanceProps) {
    const displayText = (st: Status): string | JSX.Element => {
        switch(st){
            case Status.START:
                return "ETF Performance Information will show up here"
            case Status.LOADING:
                return "Loading..."
            case Status.ERROR:
                return ("Error, try again with different parameters")
            case Status.LOADED:
                return(
                    <>
                        <div className={"Performance font"}> 
                            ETF Performance: {convertToPercentage(props.etfPerformance.toString())}
                        </div>
                    </>
                )
            default:
                return ""
        }
    }
    return(
        <div className={"Performance font"}>
               {displayText(props.status)}
        </div>
        )

}